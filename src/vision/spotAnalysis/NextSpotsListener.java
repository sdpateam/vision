package vision.spotAnalysis;

import java.util.ArrayList;
import java.util.HashMap;

import vision.colorAnalysis.SDPColor;
import vision.spotAnalysis.approximatedSpotAnalysis.Spot;

public interface NextSpotsListener {
	public void nextSpots(HashMap<SDPColor, ArrayList<Spot>> spots);
}
