package vision;


/**
 * Created by Simon on 3/5/2016.
 */
public class Ball extends PitchObject {
    public Ball(){ }

    @Override
    public Ball clone(){
        Ball ball = new Ball();
        ball.location = location == null ? null : location.clone();
        ball.velocity = velocity == null ? null : velocity.clone();
        return ball;
    }
}
