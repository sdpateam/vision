package vision;

import vision.gui.SDPConsole;
import vision.tools.DirectedPoint;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Simon on 3/5/2016.
 */
public class DynamicWorld {
    private HashMap<RobotType, Robot> robots;
    private Ball ball;
    private Ball lastKnownBall;
    private RobotType probableBallHolder;

    public DynamicWorld(){
        this.robots = new HashMap<RobotType, Robot>();
    }

    public Robot getRobot(RobotType type){
        if(this.robots.containsKey(type)) return this.robots.get(type);
        return null;
    }

    public Ball getBall(){
        return this.ball;
    }

    public void setBall(Ball ball){
        this.ball = ball;
    }

    public Ball getLastKnownBall(){
        return this.lastKnownBall;
    }

    public void setLastKnownBall(Ball ball){
        this.lastKnownBall = ball;
    }

    public RobotType getProbableBallHolder(){
        return this.probableBallHolder;
    }

    public void setRobot(Robot r){
        this.robots.put(r.type, r);
    }


    public void setProbableBallHolder(RobotType type){
        this.probableBallHolder = type;
    }

    public void printData() {
		DirectedPoint p;
		if(this.ball != null){
			SDPConsole.writeln("BALL at " + this.ball.location.x + " : " + this.ball.location.y);
		}
		for(RobotType rt : this.robots.keySet()){
			p = this.robots.get(rt).location;
			SDPConsole.writeln("ROBOT: " + rt + " at " + p.x + " : " + p.y + " heading: " + p.direction);
		}
		if(this.probableBallHolder != null) SDPConsole.writeln("Probable ball holder: " + this.probableBallHolder.toString());
        if(this.lastKnownBall != null) SDPConsole.writeln("Last Known ball: " + this.lastKnownBall.toString());
	}

    public Collection<Robot> getRobots(){
        return this.robots.values();
    }
}
