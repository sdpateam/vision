package vision.settings;

public interface SaveLoadCapable {
	public String saveSettings();
	public void loadSettings(String settings);
}
