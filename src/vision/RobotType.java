package vision;

/**
 * Created by s1351669 on 04/03/16.
 */
public enum RobotType {
    FRIEND_1, FRIEND_2, FOE_1, FOE_2;


    public RobotType[] getFriends() {
        return new RobotType[] {FRIEND_1, FRIEND_2};
    }

    public RobotType[] getFoes() {
        return new RobotType[] {FOE_1, FOE_2};
    }
}
