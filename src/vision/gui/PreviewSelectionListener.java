package vision.gui;

import vision.tools.ColoredPoint;

public interface PreviewSelectionListener {
	public void previewClickHandler(ColoredPoint coloredPoint);
}
