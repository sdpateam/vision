package vision.gui;

import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JToggleButton;

import vision.robotAnalysis.SDPColorSelector;

public class WBTest extends JFrame implements ActionListener{
	
	public static void main(String[] args){
		new WBTest();
	}
	
	private JTextField textField;
	private JTextField textField_1;
	public WBTest() {
		setTitle("Distortion");
		
		Panel panel = new Panel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		SDPColorSelector topLeft = new SDPColorSelector();
		topLeft.setBounds(10, 95, 217, 29);
		panel.add(topLeft);
		
		JLabel lblFront = new JLabel("FRONT");
		lblFront.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFront.setBounds(194, 45, 101, 15);
		panel.add(lblFront);
		
		SDPColorSelector bottomLeft = new SDPColorSelector();
		bottomLeft.setBounds(10, 175, 217, 29);
		panel.add(bottomLeft);
		
		SDPColorSelector bottomRight = new SDPColorSelector();
		bottomRight.setBounds(237, 175, 217, 29);
		panel.add(bottomRight);
		
		SDPColorSelector topRight = new SDPColorSelector();
		topRight.setBounds(237, 95, 217, 29);
		panel.add(topRight);
		
		SDPColorSelector teamColor = new SDPColorSelector();
		teamColor.setBounds(124, 135, 217, 29);
		panel.add(teamColor);
		
		JToggleButton detect = new JToggleButton("Detect : OFF");
		detect.setBounds(10, 268, 121, 23);
		panel.add(detect);
		this.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
