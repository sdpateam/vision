package vision.robotAnalysis;

import vision.DynamicWorld;

public interface DynamicWorldListener {
	public void nextDynamicWorld(DynamicWorld state);
}
