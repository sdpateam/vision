package vision.robotAnalysis.newRobotAnalysis;

import vision.Ball;
import vision.DynamicWorld;
import vision.Robot;
import vision.colorAnalysis.SDPColor;
import vision.robotAnalysis.RobotAnalysisBase;
import vision.RobotType;
import vision.spotAnalysis.approximatedSpotAnalysis.Spot;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by s1351669 on 08/03/16.
 */
public class NewRobotAnalysis extends RobotAnalysisBase {


//    private SDPColor[] teamColor = {SDPColor.YELLOW, SDPColor.BLUE};
//    private SDPColor[] spotColor = {SDPColor.GREEN, SDPColor.PINK};


    public NewRobotAnalysis(){
        super();
    }


    @Override
    public void nextUndistortedSpots(HashMap<SDPColor, ArrayList<Spot>> spots) {
        ArrayList<Spot> spotList;
        ArrayList<RobotPlate> plates = new ArrayList<RobotPlate>();

        PatternMatcher.patternMatch(spots.get(SDPColor.GREEN), plates);
        PatternMatcher.patternMatch(spots.get(SDPColor.PINK), plates);

        PatternMatcher.singularValidate(spots.get(SDPColor.GREEN), plates);
        PatternMatcher.singularValidate(spots.get(SDPColor.PINK), plates);

        PatternMatcher.removeInvalid(plates);

        PatternMatcher.teamAnalysis(plates, spots.get(SDPColor.YELLOW));
        PatternMatcher.teamAnalysis(plates, spots.get(SDPColor.BLUE));

        DynamicWorld world = new DynamicWorld();
        Robot r;

        for(RobotPlate plate : plates){
            if(!plate.hasTeam()){
                plate.setTeam(SDPColor.YELLOW);
            }
            r = plate.toRobot();
            world.setRobot(r);
        }

        spotList = spots.get(SDPColor._BALL);

        for(int i = 0; i < spotList.size(); i++){
            Spot s = spotList.get(i);
            if(PatternMatcher.isBotPart(plates, s)){
                spotList.remove(i);
                i--;
            }
        }

        Ball ball;
        Ball oldBall = null;
        if(lastKnownWorld != null) oldBall = lastKnownWorld.getBall();
        if(spotList.size() > 0){
            ball = new Ball();
            ball.location = spotList.get(0);
            world.setBall(ball);
        }
        ball = world.getBall();

        if(ball == null){
            if(lastKnownWorld != null){
                world.setProbableBallHolder(lastKnownWorld.getProbableBallHolder());
                world.setLastKnownBall(lastKnownWorld.getLastKnownBall());
            }
        } else {
            world.setLastKnownBall(ball);
            for(Robot robot : world.getRobots()){
                if(PatternMatcher.posessesBall(robot, ball)){
                    world.setProbableBallHolder(robot.type);
                    break;
                }
            }
            if(oldBall == null){
                ball.velocity = new VectorGeometry(0,0);
            } else {
                ball.velocity = VectorGeometry.fromTo(oldBall.location, ball.location);
            }
        }


        if(lastKnownWorld != null){
            for(RobotType rt : RobotType.values()){
                Robot old = lastKnownWorld.getRobot(rt);
                Robot newR = world.getRobot(rt);
                if(newR != null){
                    if(old != null){
                        newR.velocity = newR.location.clone();
                        newR.velocity.minus(old.location);
                        newR.velocity.direction = newR.location.direction - old.location.direction;
                    } else {
                        newR.velocity = new DirectedPoint(0,0,0);
                    }
                }
            }
        }


        for(Robot toUndistort : world.getRobots()){
            if(toUndistort != null){
                toUndistort.location.setLength(toUndistort.location.length() * (1 - 15.0/250));
            }
        }
        if(world.getRobot(RobotType.FRIEND_2) != null) this.informListeners(world);
    }
}
