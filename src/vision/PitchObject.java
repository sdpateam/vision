package vision;

import vision.tools.VectorGeometry;

abstract public class PitchObject {

    public VectorGeometry location;
    public VectorGeometry velocity;

    abstract public PitchObject clone();
}
