package vision.distortion;

public interface DistortionPreviewClickListener {
	public void distortionPreviewClickHandler(int x, int y);
}
