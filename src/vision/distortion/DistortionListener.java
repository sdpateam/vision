package vision.distortion;

import java.util.ArrayList;
import java.util.HashMap;

import vision.colorAnalysis.SDPColor;
import vision.spotAnalysis.approximatedSpotAnalysis.Spot;

public interface DistortionListener {
	public void nextUndistortedSpots(HashMap<SDPColor, ArrayList<Spot>> spots);
}
