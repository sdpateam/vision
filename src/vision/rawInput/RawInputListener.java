package vision.rawInput;

import java.awt.image.BufferedImage;

public interface RawInputListener {
	public void nextFrame(BufferedImage image);
}
