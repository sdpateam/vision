package vision.rawInput;

public interface RawInputInterface {
	public void stop();
	public void start();
}
