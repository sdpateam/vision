package vision.tools;

/**
 * Created by Simon on 2/24/2016.
 */
public class VectorGeometry {
    public double x;
    public double y;

    public VectorGeometry(){}

    public VectorGeometry(double x, double y){
        this.x = x;
        this.y = y;
    }

    @Override
    public VectorGeometry clone(){
        return new VectorGeometry(this.x, this.y);
    }

    public double length(){
        return VectorGeometry.length(this.x, this.y);
    }

    @Override
    public String toString(){
        return "X: " + this.x + " Y: " + this.y;
    }

    public VectorGeometry copyInto(VectorGeometry v){
        v.x = this.x;
        v.y = this.y;
        return v;
    }

    public VectorGeometry plus(double x, double y){
        this.x = this.x + x;
        this.y = this.y + y;
        return this;
    }

    public VectorGeometry plus(VectorGeometry vector){
        return this.plus(vector.x, vector.y);
    }

    public VectorGeometry minus(double x, double y){
        this.x = this.x - x;
        this.y = this.y - y;
        return this;
    }

    public VectorGeometry factor(double factor){
        this.x = this.x*(1 + factor);
        this.y = this.y*(1 + factor);
        return this;
    }

    public VectorGeometry minus(VectorGeometry vector){
        return this.minus(vector.x, vector.y);
    }



    public VectorGeometry multiply(double factor){
        this.x = this.x * factor;
        this.y = this.y * factor;
        return this;
    }

    public double angle(){
        return VectorGeometry.angle(this.x, this.y);
    }

    public VectorGeometry add(double x, double y){
        this.x = this.x + x;
        this.y = this.y + y;
        return this;
    }

    public VectorGeometry add(VectorGeometry v){
        return this.add(v.x, v.y);
    }

    public VectorGeometry setLength(double len){
        double length = this.length();
        if(length == 0) return this;
        double factor = len / length;
        this.x = this.x * factor;
        this.y = this.y * factor;
        return this;
    }

    public VectorGeometry fromAngular(double d, double distance){
        this.x = distance*Math.cos(d);
        this.y = distance*Math.sin(d);
        return this;
    }

    public static double angle(VectorGeometry a, VectorGeometry b, VectorGeometry c){
        VectorGeometry ba = a.copyInto(new VectorGeometry()).minus(b);
        VectorGeometry bc = c.copyInto(new VectorGeometry()).minus(b);
        return VectorGeometry.angle(ba, bc);
    }

    public static double angle(VectorGeometry a, VectorGeometry b){
        return VectorGeometry.angle(a.x,a.y,b.x,b.y);
    }

    public double distance(double x, double y){
        return VectorGeometry.distance(this.x, this.y, x, y);
    }

    public double distance(VectorGeometry other){
        return this.distance(other.x, other.y);
    }

    public VectorGeometry rotate(double d){
        this.x = Math.cos(d)*this.x - Math.sin(d)*this.y;
        this.y = Math.sin(d)*this.x + Math.cos(d)*this.y;
        return this;
    }

    public VectorGeometry coordinateRotation(double phi){
//        this.x = Math.cos(d)*this.x + Math.sin(d)*this.y;
//        this.y = -Math.sin(d)*this.x + Math.cos(d)*this.y;
        double length = this.length();
        this.x = Math.cos(phi)*length;
        this.y = Math.sin(phi)*length;
        return this;
    }

    public VectorGeometry transpose(double dx, double dy){
        return this.plus(dx, dy);
    }



    public static VectorGeometry fromAngular(double d, double distance, VectorGeometry vg){
        if(vg == null) vg = new VectorGeometry();
        return vg.fromAngular(d, distance);
    }



    public VectorGeometry tilt3D(double deltaX, double deltaY){
        this.x = this.x * (1 + deltaX*this.y);
        this.y = this.y * (1 + deltaY*this.x);
        return this;
    }

    public VectorGeometry barrelUndistort(double barrelConstant){
        double distortedDistance = this.length();
        double undistortedDistance = distortedDistance/(1-barrelConstant*distortedDistance*distortedDistance);
        this.fromAngular(this.angle(), undistortedDistance);
        return this;
    }

    public static double distance(double x1, double y1, double x2, double y2){
        return Math.sqrt(squareDistance(x1, y1, x2, y2));
    }

    public static double squareDistance(double x1, double y1, double x2, double y2){
        double x = x1 - x2;
        double y = y1 - y2;
        return x*x + y*y;
    }

    public static double squareLength(double x, double y){
        return squareDistance(0,0,x,y);
    }

    public static double length(double x, double y){
        return Math.sqrt(squareLength(x, y));
    }

    public static double angle(double x1, double y1, double x2, double y2){
        double cos = (x1*x2 + y1*y2)/(length(x1, y1)*length(x2, y2));
        if(cos > 1) return 0;
        if(cos < -1) return Math.PI;
        return Math.acos(cos);
    }

    public static double angle(double x, double y){
        double res = angle(1,0,x,y);
        if(y < 0) return -res;
        return res;
    }

    public static VectorGeometry closestPointToLine(VectorGeometry point, VectorGeometry base, VectorGeometry dir){
        double numerator = dotProduct(point, dir) - dotProduct(base, dir);
        double denominator = dotProduct(dir, dir);
        double t = numerator/denominator;
        VectorGeometry vector = new VectorGeometry();
        vector.x = base.x + t*dir.x;
        vector.y = base.y + t*dir.y;
        return vector;
    }

    public static boolean isInGeneralDirection(VectorGeometry base, VectorGeometry dir, VectorGeometry point){
        VectorGeometry baseToPoint = new VectorGeometry(point.x - base.x, point.y - base.y);
        double angle = Math.abs(VectorGeometry.angle(dir.x, dir.y, baseToPoint.x, baseToPoint.y));
        return angle < 1;
    }

    public static VectorGeometry vectorToClosestPointOnFiniteLine(VectorGeometry a, VectorGeometry b, VectorGeometry point){
        VectorGeometry dir = new VectorGeometry();
        dir.x = b.x-a.x;
        dir.y = b.y-a.y;
        VectorGeometry closest = VectorGeometry.closestPointToLine(point, a, dir);
        if(VectorGeometry.isBetweenPoints(closest, a, b)){
            return closest;
        }
        if(VectorGeometry.distance(closest, a) < VectorGeometry.distance(closest,b)){
            a.copyInto(closest);
        } else {
            b.copyInto(closest);
        }
        return closest;
    }

    public static VectorGeometry intersectionWithFiniteLine(VectorGeometry point, VectorGeometry pointDir, VectorGeometry a, VectorGeometry b){
        VectorGeometry intersection = intersectionOfLines(point, pointDir, a, VectorGeometry.fromTo(a, b));
        if(intersection == null) return null;
        if(isBetweenPoints(intersection, a, b)){
            return intersection;
        }
        return closerOfTwo(intersection, a, b);
    }

    public static VectorGeometry closerOfTwo(VectorGeometry point, VectorGeometry a, VectorGeometry b){
        if(VectorGeometry.distance(point, a) > VectorGeometry.distance(point, b)){
            return b;
        } else {
            return a;
        }
    }


    public static double distanceFromLine(VectorGeometry point, VectorGeometry base, VectorGeometry dir){
        VectorGeometry vector = closestPointToLine(point, base, dir);
        return vector.distance(vector.x, vector.y);
    }

    public static double dotProduct(VectorGeometry v1, VectorGeometry v2){
        return v1.x*v2.x + v1.y*v2.y;
    }

    public static VectorGeometry intersectionOfLines(VectorGeometry a, VectorGeometry x, VectorGeometry b, VectorGeometry y){
        double discriminant = x.y*y.x - x.x*y.y;
        if(discriminant == 0) return null;
        double t = (-y.y*(b.x-a.x) + y.x*(b.y-a.y))/discriminant;
        VectorGeometry res = new VectorGeometry();
        res.x = a.x + t*x.x;
        res.y = a.y + t*x.y;
        return res;
    }

    public static double distance(VectorGeometry a, VectorGeometry b){
        return VectorGeometry.distance(a.x,a.y,b.x,b.y);
    }

    public static boolean isBetweenPoints(VectorGeometry point, VectorGeometry a, VectorGeometry b){
        return VectorGeometry.distance(a,b) + 1 > VectorGeometry.distance(point, a) + VectorGeometry.distance(point, b);
    }

    public static void main(String[] args){
//        System.out.println(VectorGeometry.intersectionWithFiniteLine(
//                new VectorGeometry(0,0),
//                new VectorGeometry(-1,2),
//                new VectorGeometry(0,2),
//                new VectorGeometry(2,0)
//        ));
        System.out.println(VectorGeometry.signedAngle(new VectorGeometry(1,0), new VectorGeometry(0,1)));
        System.out.println(VectorGeometry.signedAngle(new VectorGeometry(0,1), new VectorGeometry(1,0)));
    }

    public static boolean crossProductDirection(VectorGeometry a, VectorGeometry b){
        double res = a.x*b.y - a.y*b.x;
        return res < 0;
    }


    /**
     * Returns angle from b to a.
     * @param a
     * @param b
     * @return
     */
    public static double signedAngle(VectorGeometry a, VectorGeometry b){
        double angle = VectorGeometry.angle(a,b);
        boolean sign = crossProductDirection(a, b);
        return angle * (sign ? 1 : -1);
    }

    public static VectorGeometry fromTo(VectorGeometry a, VectorGeometry b){
        return b.copyInto(new VectorGeometry()).minus(a);
    }

    public static boolean isInRectangle(VectorGeometry point, double x1, double y1, double x2, double y2){
        return point.x > x1 && point.y > y1 && point.x < x2 && point.y < y2;
    }

    public static VectorGeometry fromTo(double x, double y, int x1, int y1) {
        return fromTo(new VectorGeometry(x, y), new VectorGeometry(x1, y1));
    }
}
