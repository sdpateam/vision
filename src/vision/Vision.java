package vision;

import java.awt.BorderLayout;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import vision.colorAnalysis.ColorCalibration;
import vision.tools.CommandLineParser;
import vision.distortion.Distortion;
import vision.distortion.DistortionPreview;
import vision.gui.MiscelaneousSettings;
import vision.gui.Preview;
import vision.gui.SDPConsole;
import vision.rawInput.RawInput;
import vision.robotAnalysis.newRobotAnalysis.NewRobotAnalysis;
import vision.robotAnalysis.RobotPreview;
import vision.robotAnalysis.DynamicWorldListener;
import vision.robotAnalysis.RobotAnalysisBase;
import vision.spotAnalysis.SpotAnalysisBase;
import vision.spotAnalysis.approximatedSpotAnalysis.ApproximatedSpotAnalysis;
import vision.spotAnalysis.recursiveSpotAnalysis.RecursiveSpotAnalysis;

public class Vision extends JFrame implements DynamicWorldListener {

	// VISION TO DO LIST:
	// TODO: Add robot pattern setup in whatever simple way remains
	// TODO: Add subtraction image in the command line arguments
	// TODO: Add a GUI for image subtraction
	// TODO: Add a command line argument for static image input start
	// TODO: Add deduction system to find most probable stuff.
	// TODO: Add zoom option to the Distortion.
	
	private LinkedList<VisionListener> visionListeners;
	
	/**
	 * Add a vision listener. The Listener will be notified whenever the
	 * vision system has a new static world.
	 * @param visionListener
	 */
	public void addVisionListener(VisionListener visionListener){
		this.visionListeners.add(visionListener);
	}
	
	/**
	 * Vision system constructor. Please please please only call this once. I will eventually make it a singleton.
	 */
	public Vision(String[] args){
		super("Vision");
		
		this.visionListeners   = new LinkedList<VisionListener>();
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

		SpotAnalysisBase recursiveSpotAnalysis   = new RecursiveSpotAnalysis();
		SpotAnalysisBase approximateSpotAnalysis = new ApproximatedSpotAnalysis();


		// Build the vision system pipeline
		RawInput.addRawInputListener(recursiveSpotAnalysis);
		RawInput.addRawInputListener(Preview.preview);
		RawInput.addRawInputListener(Distortion.distortion);
		recursiveSpotAnalysis.addSpotListener(Distortion.distortion);
		DistortionPreview.addDistortionPreviewClickListener(Distortion.distortion);
		Distortion.addDistortionListener(RobotPreview.preview);

		RobotAnalysisBase robotAnalysis = new NewRobotAnalysis();
		Distortion.addDistortionListener(robotAnalysis);
		robotAnalysis.addDynamicWorldListener(RobotPreview.preview);
		robotAnalysis.addDynamicWorldListener(this);
		
		
		tabbedPane.addTab("Input Selection", null, RawInput.rawInputMultiplexer, null);
		tabbedPane.addTab("Color Calibration", null, ColorCalibration.colorCalibration, null);
		tabbedPane.addTab("Distortion", null, Distortion.distortion, null);
//		tabbedPane.addTab("Robots", null, RobotAnalysis.robots, null);
		tabbedPane.addTab("Misc Settings", null,  MiscelaneousSettings.miscSettings, null);
		
		SDPConsole.console.setVisible(true);
		
		this.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		this.setSize(640,480);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				terminateVision();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		CommandLineParser.parser.newParse(args, this);
		this.setVisible(true);
	}

	/**
	 * Call this function to safely turn off all the Vision stuff.
	 */
	public void terminateVision(){
		RawInput.rawInputMultiplexer.stopAllInputs();
	}

	public static void main(String[] args){
		new Vision(args);
	}

	@Override
	public void nextDynamicWorld(DynamicWorld state) {
		for(VisionListener visionListener : this.visionListeners){
			visionListener.nextWorld(state);
		}
	}
}