package vision.constants;

public class Constants {
	public static final int INPUT_WIDTH  = 640; // In pixels
	public static final int INPUT_HEIGHT = 480; // In pixels
	public static final int PITCH_WIDTH  = 300; // In centimetres
	public static final int PITCH_HEIGHT = 220; // In centimetres
	public static boolean   GUI          = true;
	public static boolean   TIMER        = false;
//	public static final int PREVIEW_FACTOR = 2;
}
