package vision;

import vision.tools.DirectedPoint;

/**
 * Created by Simon on 3/5/2016.
 */
public class Robot extends PitchObject {
    public DirectedPoint location;
    public DirectedPoint velocity;
    public RobotType type;

    public Robot() {

    }

    @Override
    public Robot clone() {
        Robot r = new Robot();
        r.location = location == null ? null : location.clone();
        r.velocity = velocity == null ? null : velocity.clone();
        r.type = type;
        return r;
    }
}
